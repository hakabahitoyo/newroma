<!--
### Precheck

* Please do a quick search to ensure no similar issues has been reported before. If the issue has not been addressed after 2 weeks, it's fine to bump it (e.g. give a thumbs up or leave a comment).
* Try to ensure that the bug is actually related to the Pleroma backend. For example, if a bug happens in Pleroma-FE but not in Mastodon-FE or mobile clients, it's likely that the bug should be filed in [Pleroma-FE](https://git.pleroma.social/pleroma/pleroma-fe/issues/new) repository.
-->

### Environment (only relevant for bugs, not enhancement requests)

* Installation type (OTP or From Source):
* Version (could be found in the "Version" tab of settings in Pleroma-FE): 
* Elixir version (`elixir -v` for from source installations, N/A for OTP):
* Operating system:
* PostgreSQL version (`psql -V`):


### Description

<!--
Please try to be clear what the problem is.
* For bugs, add steps to reproduce, what's the result and what you expect. Logfiles, screenshots and additional information can also help us better understand the problem.
* For enhancements, make it clear what you expect and what problem you try to solve.

You can link to other issues or pull requests by using a hashtag and number. E.g. to refer to issue https://codeberg.org/newroma-dev/newroma/issues/32 you can write #32
-->
